# Bibliotheka Chymerica

This is an script suite for generating beautiful `.pdf` documents for various books, including plain-text reference files for the respective books.

*This is a work in progress, no books are currently complete.*
Any bibliophile who like reading on displays and is appalled by the abysmal quality of electronic document typesetting, is welcome to contribute.

## Collection

The collection of books is published under `books.chymera.eu/`, followed by a directory corresponding to the (abbreviated) author name, and the file name corresponding to the (abbreviated) book name.

So for Johann Wolfgang Goethe's “Faust, eine Tragödie”, the URL is [books.chymera.eu/jwg/f1.pdf](https://books.chymera.eu/jwg/f1.pdf).
