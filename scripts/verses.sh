#!/usr/bin/env bash

IN_FILE=${1}
#OUT_FILE=${IN_FILE%%*.}

if [ ${IN_FILE##*.md} ]; then
	echo "Input file must be markdown and end with \`.md\`."
	echo "\`${IN_FILE}\` does not satisfy this requirement."
	exit
fi

#SCRIPT_DIR=$(dirname "$0")
#OUT_DIR="${SCRIPT_DIR}/../inputs"
OUT_DIR=${2}
WORK_DIR="/tmp/freebrary/$(whoami)/$(date +%Y%m%d%H%M%S)"

OUT_FILE="${OUT_DIR}/$(basename ${IN_FILE%%.md*}.tex)"
WORK_FILE="${WORK_DIR}/$(basename ${IN_FILE%%.md*}.work)"

echo "${SCRIPT_DIR}"
echo "${WORK_DIR}"
echo "${OUT_DIR}"
echo "${IN_FILE}"
echo "${WORK_FILE}"
echo "${OUT_FILE}"

mkdir -p "${WORK_DIR}"
cp "${IN_FILE}" ${WORK_FILE}

#sed -e "s///g" ${1} &> "${1}"
#sed -e "s/^## (?P<boldline>.+)$/\g<boldline>/g" ${1}
#sed -e "s/^## \(\?P<b>.*\)$/lsls/g" ${1}

pushd ${WORK_DIR}
# Use soft hyphen
#sed -e 's/-/\\-/g' -i ${WORK_FILE}
# Bold lines for characters.
#sed -e 's/^## \(.*\)$/\\textbf{\1}\n/g' -i ${WORK_FILE}
sed -e 's/^#### \(.*\)$/\\textbf{\1}\\nopagebreak[4]\n\\begin{verse}/g' -i ${WORK_FILE}
#sed -e 's/^## \(.*\)$/\\textbf{\1}\n\\begin{verse}/g' -i ${WORK_FILE}
#sed -e 's/^## \(.*\)$/\\textbf{\1}\n/g' -i ${WORK_FILE}
#cat ${WORK_FILE}


# Part title
sed -e 's/^# \(.*\)$/\\part{\1}/g' -i ${WORK_FILE}
#cat ${WORK_FILE}

# Chapter title
sed -e 's/^## \(.*\)$/\\chapter{\1}/g' -i ${WORK_FILE}
#cat ${WORK_FILE}

# Manual indentation
sed -e 's/^\t\t/\\qquad\\qquad\ /g' -i ${WORK_FILE}
sed -e 's/^\t/\\quad\ /g' -i ${WORK_FILE}
#cat ${WORK_FILE}

# Break up verses
sed -e 's/^$/\\begin{verse}/g' -i ${WORK_FILE}
awk -i inplace '/\\begin{verse}/&&v++%2{sub(/\\begin{verse}/, "\\end{verse}")}{print}' ${WORK_FILE}
#awk -i inplace '/^$/&&v++%2{sub(/^$/, "lulu")}{print}' ${WORK_FILE}
#cat "${WORK_FILE}"

sed -e 's/^### \(.*\)$/{\\centering\\spacedshape\\large \1\\par}\n\\nopagebreak[4]\\vspace{2em}\n\\nopagebreak[4]/g' -i ${WORK_FILE}
sed -e 's/^##### \(.*\)$/{\\centering\\small \1\\par}\n\\vspace{.5em}\n/g' -i ${WORK_FILE}

popd

cp "${WORK_FILE}" ${OUT_FILE}
rm -rf "${WORK_DIR}"
