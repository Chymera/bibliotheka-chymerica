#!/usr/bin/env bash

inputs/jwg/f1.tex: reference/jwg/f1.md
	scripts/verses.sh reference/jwg/f1.md inputs/jwg/

jwg/f1: inputs/jwg/f1.tex
	mkdir -p bc/jwg
	cd sources/jwg/ ; rubber --pdf -m xelatex f1.tex && if [ -f "f1.pdf" ]; then mv f1.pdf ../../bc/jwg/; fi

clean:
	find . -name '*.aux' -exec rm -f {} \;
	find . -name '*.log' -exec rm -f {} \;
	find . -name '*.out' -exec rm -f {} \;
	find . -name '*.toc' -exec rm -f {} \;

clean-full:
	find . -name '*.rubbercache' -exec rm -f {} \;

publish:
	rsync -avP bc/* dreambooks:books.chymera.eu/
